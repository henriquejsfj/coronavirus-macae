# Coronavirus Macae

Micro projeto de análise dos casos de coronavírus em Macaé. 
Os dados foram reunidos manualmente dos informes diários da prefeitura. Uma predição para os 10 dias com base em ajuste de uma função exponencial foi feito.

Os dados estão nesse [link](https://docs.google.com/spreadsheets/d/1JbCEm0LWKC9saFcCg5Dy0h8P0CH5MohoO_5W1ZtX24U/edit#gid=0) e podem ser baixados como csv para uso no cód.

A estimativa da previsão se encontra no arquivo [CoronavirusMacae.ipynb](./CoronavirusMacae.ipynb). 

É claro que o ajuste por exponencial feito aqui é uma grosseria, por isso a previsão é de um prazo curto de 10 dias. Para mais informações sobre ajuste de modelos ao coronavírus leia esse [artigo](https://medium.com/data-for-science/epidemic-modeling-101-or-why-your-covid19-exponential-fits-are-wrong-97aa50c55f8).
